# kaggle_tokyo_stock_exchange_prediction

Projeto desenvolvido para acompanhar a evolução na competição de Predição do mercado JPX de ações e opções de Tokio, no Kaggle.

## Descrição

O sucesso em qualquer mercado financeiro exige que se identifique investimentos sólidos. Quando uma ação ou derivativo está subvalorizado, faz sentido comprar. Se estiver supervalorizado, talvez seja hora de vender. Embora essas decisões financeiras tenham sido historicamente feitas manualmente por profissionais, a tecnologia trouxe novas oportunidades para investidores de varejo. Os cientistas de dados, especificamente, podem estar interessados ​​em explorar o comércio quantitativo, onde as decisões são executadas programaticamente com base em previsões de modelos treinados.

Existem muitos esforços de negociação quantitativa existentes usados ​​para analisar os mercados financeiros e formular estratégias de investimento. Criar e executar tal estratégia requer dados históricos e em tempo real, o que é difícil de obter especialmente para investidores de varejo. Essa competição fornecerá dados financeiros para o mercado japonês, permitindo que os investidores de varejo analisem o mercado ao máximo.

A Japan Exchange Group, Inc. (JPX) é uma holding que opera uma das maiores bolsas de valores do mundo, a Tokyo Stock Exchange (TSE), e as bolsas de derivativos Osaka Exchange (OSE) e Tokyo Commodity Exchange (TOCOM). A JPX está hospedando esta competição e é apoiada pela empresa de tecnologia de IA AlpacaJapan Co.,Ltd.

Esta competição comparará seus modelos com retornos futuros reais após a conclusão da fase de treinamento. A competição envolverá a construção de carteiras a partir das ações elegíveis para previsões (cerca de 2.000 ações). Especificamente, cada participante classifica as ações do retorno esperado mais alto para o mais baixo e é avaliado pela diferença de retornos entre as 200 melhores e as últimas 200 ações. Você terá acesso a dados financeiros do mercado japonês, como informações de ações e preços históricos de ações para treinar e testar seu modelo.

Todos os modelos vencedores serão tornados públicos para que outros participantes possam aprender com os modelos em destaque. Modelos excelentes também podem aumentar o interesse no mercado entre investidores de varejo, inclusive aqueles que desejam praticar negociação quantitativa. Ao mesmo tempo, você obterá seus próprios insights sobre métodos de investimento programático e análise de portfólio – e poderá até descobrir que tem afinidade com o mercado japonês.

## Avaliação

As submissões são avaliadas pelo Índice de Sharpe dos retornos diários do spread. Você precisará classificar cada ação ativa em um determinado dia. Os retornos para um único dia tratam as 200 ações com classificação mais alta (por exemplo, 0 a 199) como compradas e as mais baixas (por exemplo, 1999 a 1800) classificaram 200 ações como vendidas. As ações são então ponderadas com base em suas classificações e os retornos totais do portfólio são calculados assumindo que as ações foram compradas no dia seguinte e vendidas no dia seguinte. Você pode encontrar uma implementação python da métrica aqui.

Você deve se inscrever nesta competição usando a API de série temporal python fornecida, que garante que os modelos não apareçam no tempo. Para usar a API, siga este modelo no Kaggle Notebooks:

```python
import jpx_tokyo_market_prediction
env = jpx_tokyo_market_prediction.make_env()   # initialize the environment
iter_test = env.iter_test()    # an iterator which loops over the test files
for (prices, options, financials, trades, secondary_prices, sample_prediction) in iter_test
    sample_prediction_df['Rank'] = np.arange(len(sample_prediction))  # make your predictions here
    env.predict(sample_prediction_df)   # register your predictions
```

Você receberá um erro se:

- Usar classificações abaixo de zero ou maiores ou iguais ao número de ações para uma determinada data.
- Enviar quaisquer classificações duplicadas.
- Alterar a ordem das linhas.

## Timeline

Cronograma de previsão:

A partir do prazo final de envio, haverá atualizações periódicas na tabela de classificação para refletir as atualizações de dados de mercado que serão executadas em notebooks selecionados. As atualizações ocorrerão aproximadamente a cada duas semanas.

### 7 de outubro de 2022 - Data de término da competição - Anúncio do vencedor

## Requisitos de código

### Esta é uma competição de código

As inscrições para este concurso devem ser feitas através de Notebooks. Para que o botão "Enviar" fique ativo após um commit, as seguintes condições devem ser atendidas:

- CPU Notebook <= 9 horas de tempo de execução
- Notebook GPU <= 9 horas de tempo de execução
- Acesso à Internet desativado
- Dados externos disponíveis gratuitamente e publicamente são permitidos, incluindo modelos pré-treinados
- O arquivo de envio deve ser nomeado submit.csv. A API irá gerar este arquivo de envio para você.

Consulte as Perguntas frequentes sobre a competição de códigos para obter mais informações sobre como enviar. E revise o documento de depuração de código se estiver encontrando erros de envio.

## Descrição dos dados

Este conjunto de dados contém dados históricos para uma variedade de ações e opções japonesas. Seu desafio é prever os retornos futuros das ações.

Como os preços históricos das ações não são confidenciais, esta será uma competição de previsão usando a API de séries temporais. Os dados para o período da tabela de classificação pública são incluídos como parte do conjunto de dados da competição. Espere ver muitas pessoas enviando envios perfeitos por diversão. Assim, a tabela de classificação pública da fase ativa para esta competição pretende ser uma conveniência para quem deseja testar seu código. A tabela de classificação da fase de previsão será determinada usando dados de mercado reais coletados após o término do período de envio.

## Autores

@almir
@leonardo
@matheus

## License

For open source projects, say how it is licensed:
Open Source Licence.
